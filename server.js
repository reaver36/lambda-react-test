const express = require('express');

function createServer(app) {
  const handle = app.getRequestHandler();
  const server = express();
  // add middleware, custom routing, whatever
  server.get('*', (req, res) => {
    req.url = req.url.replace(/\/?test\//, '');
    console.log(req.url);
    console.log(req);
    handle(req, res);
  });
  return server;
}

module.exports = createServer;