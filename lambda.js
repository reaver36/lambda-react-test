process.env.IN_LAMBDA = "1";
process.env.NODE_ENV = "production";
const next = require('next');

const dev = process.env.NODE_ENV !== 'production';
const nextApp = next({ dev });
const awsServerlessExpress = require('aws-serverless-express');
exports.handler = (event, context) => {
    nextApp.prepare().then(function(){
        const app = require('./server')(nextApp);
        const server = awsServerlessExpress.createServer(app);
        awsServerlessExpress.proxy(server, event, context);
    });
}