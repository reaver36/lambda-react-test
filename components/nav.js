import Link from 'next/link'

const nav = () => (
  <div>
    <Link href="/index">
      <a>Home</a>
    </Link>
    <Link href="/other">
      <a>Other Page</a>
    </Link>
  </div>
)

export default nav